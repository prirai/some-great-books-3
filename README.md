# some-great-books-3
Prescribed books for the third semester, all in one place. Feel free to contribute to this repository. Checkout [IISERB](https://prirai.github.io/iiser.html) page for more of this and also a link to all things good.

The Gdrive link for BS-MS books is [here](https://drive.google.com/drive/folders/1CiGzNM3Ng-j-2feU-s9ilfVYtkEDF1pY), some of the books which couldn't be uploaded here can be found on this drive.

* BIO books can be found [here](https://github.com/PeskyBirdie/Bio-Third-Semester).

First Sem books : https://iiser-my.sharepoint.com/:f:/g/personal/priyanshu20_iiserb_ac_in/EuvIdLAltDdNulcoAaLs8PoBGF5DAhOkYGMSCM1AFIIT9Q

Second Sem books : https://iiser-my.sharepoint.com/:f:/g/personal/priyanshu20_iiserb_ac_in/Ej0oecG_EM9Mj0zy08luaVsBCn8wibMolmZRaq8V573UMQ

